class MobileNumbers {
  static String parse([String mobileNumber, String countryPrefix = "263"]) {
    if (countryPrefix != "263") {
      throw ArgumentError('Only country prefix 263 supported.');
    }

    if (mobileNumber == null) {
      throw ArgumentError('Mobile number cannot be empty');
    }

    String trimmed = mobileNumber.replaceAll(RegExp(r"\s+\b|\b\s|\s|\b"), "");

    if (trimmed == '') {
      throw ArgumentError('Mobile number cannot be empty');
    }

    String pattern = r'^\+?(?:263)?0?((?:71|73|77|78)\d{7})$';

    RegExp regex = RegExp(pattern);

    if (!regex.hasMatch(trimmed)) {
      throw ArgumentError('Mobile number is invalid');
    }

    return '+263${regex.firstMatch(trimmed).group(1)}';
  }
}
