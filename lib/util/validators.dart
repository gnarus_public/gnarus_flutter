import 'mobile_number.dart';

typedef Validator = String Function(String);

class Validators {
  static Validator required({message = "This field is required"}) =>
      (value) => [null, ""].contains(value) ? message : null;

  static Validator length({int exactly, message}) => (String value) {
        if (exactly != null) {
          if (value?.length == exactly) {
            return null;
          }

          if (message == null) {
            message = 'Enter $exactly characters';
          }

          return message;
        }

        return null;
      };

  static Validator mobileNumber(
          {String message, String countryPrefix = "263"}) =>
      (value) {
        if (value == null || value.isEmpty) {
          return null;
        }
        try {
          MobileNumbers.parse(value, countryPrefix);
          return null;
        } on ArgumentError catch (e) {
          return message ?? e.message;
        }
      };

  static Validator combine(List<Validator> validators) => (value) {
        for (Validator validator in validators) {
          var result = validator(value);
          if (result != null) {
            return result;
          }
        }
        return null;
      };
}
