import 'package:intl/intl.dart';

num epochNow() => epoch(DateTime.now());

DateTime midnight(DateTime datetime) =>
    DateTime(datetime.year, datetime.month, datetime.day);

num epoch(DateTime datetime) => (datetime.millisecondsSinceEpoch);

DateTime datetime(num epoch) => DateTime.fromMillisecondsSinceEpoch(epoch);

String formatEpoch(num epoch, String format) {
  return DateFormat(format).format(datetime(epoch));
}

String formatEpochNicely(num epoch) => formatNicely(datetime(epoch));

String formatNicely(DateTime dateTime) {
  if (midnight(DateTime.now()).isAtSameMomentAs(midnight(dateTime))) {
    return "Today";
  }

  if (midnight(DateTime.now().subtract(Duration(days: 1)))
      .isAtSameMomentAs(midnight(dateTime))) {
    return "Yesterday";
  }

  return DateFormat('EEE, yyyy-MM-dd').format(dateTime);
}

DateTime addMonths(int months, DateTime dt) {
  var newDay = dt.day;
  var newMonth = dt.month + months;
  var newYear = dt.year;

  if (newMonth == 0) {
    newMonth = 12;
    newYear--;
  }

  if (newMonth == 13) {
    newMonth = 1;
    newYear++;
  }

  int maxDay = getMaxDay(newYear, newMonth);

  if (newDay > maxDay) {
    newDay = maxDay;
  }
  return DateTime(newYear, newMonth, newDay);
}

int getMaxDay(int year, int month) {
  switch (month) {
    case 2:
      if (year % 4 == 0) {
        return 29;
      }
      return 28;
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      return 31;
    case 4:
    case 6:
    case 9:
    case 11:
      return 30;
  }
  throw StateError("Invalid month: $month");
}
