import 'package:flutter/material.dart';

// TODO: move this to Dialogs class or vice versa
Future<bool> showDeleteConfirm({
  @required BuildContext context,
  String title = 'Confirm delete',
  String content,
  String entityName,
  String confirmButtonText = 'DELETE',
}) =>
    showDialog<bool>(
      context: context,
      builder: (context) => AlertDialog(
        title: Text(title),
        content: Text(content ??
            'Are you sure you want to delete this ${entityName ?? 'item'}?\n'
                '\n'
                'This action cannot be undone.'),
        actions: <Widget>[
          FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text('CANCEL'),
          ),
          RaisedButton(
            color: Colors.red,
            onPressed: () => Navigator.of(context).pop(true),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                confirmButtonText,
                style: TextStyle(color: Colors.white),
              ),
            ),
          )
        ],
      ),
    );
