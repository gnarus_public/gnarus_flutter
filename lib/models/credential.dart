import 'package:gnarus_flutter/mvvm/model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'credential.g.dart';

@JsonSerializable(explicitToJson: true)
class Credential extends Model {
  String mobileNumber;
  String emailAddress;
  String displayName;

  Credential({this.mobileNumber, this.emailAddress, this.displayName});

  factory Credential.fromJson(Map<String, dynamic> json) =>
      _$CredentialFromJson(json);

  @override
  Map<String, dynamic> toJson() => _$CredentialToJson(this);
}
