// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Credential _$CredentialFromJson(Map<String, dynamic> json) {
  return Credential(
      mobileNumber: json['mobileNumber'] as String,
      emailAddress: json['emailAddress'] as String,
      displayName: json['displayName'] as String)
    ..id = json['id'] as String
    ..created = json['created'] as int
    ..updated = json['updated'] as int
    ..deleted = json['deleted'] as int;
}

Map<String, dynamic> _$CredentialToJson(Credential instance) =>
    <String, dynamic>{
      'id': instance.id,
      'created': instance.created,
      'updated': instance.updated,
      'deleted': instance.deleted,
      'mobileNumber': instance.mobileNumber,
      'emailAddress': instance.emailAddress,
      'displayName': instance.displayName
    };
