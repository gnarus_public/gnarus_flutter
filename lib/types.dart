typedef void VoidSupplier();

typedef void ConsumerOf<T>(T input);

typedef T SupplierOf<T>();

typedef R FunctionOf<T, R>(T input);

typedef int ComparisonOf<T>(T i1, T i2);

typedef bool PredicateOf<T>(T input);

typedef T OperatorOf<T>(T input);

ComparisonOf<T> comparingNums<T>(
  FunctionOf<T, num> field, {
  bool desc = false,
}) =>
    (T i1, T i2) => desc
        ? (field(i2) ?? 0) - (field(i1) ?? 0)
        : (field(i1) ?? 0) - (field(i2) ?? 0);

ComparisonOf<T> comparingStrings<T>(
  FunctionOf<T, String> field, {
  bool desc = false,
}) =>
    (T i1, T i2) => desc
        ? (field(i2)?.compareTo(field(i1)) ?? 0)
        : (field(i1)?.compareTo(field(i2)) ?? 0);

// TODO: move this to a package
void runLater(VoidSupplier supplier) =>
    Future.delayed(Duration(milliseconds: 0), () => supplier());
