import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gnarus_flutter/mvvm.dart';
import 'package:gnarus_flutter/screens/busy_widget.dart';
import 'package:gnarus_flutter/types.dart';
import 'package:uuid/uuid.dart';

abstract class View<T extends ViewModel> extends StatefulWidget {
  final List<DeviceOrientation> preferredOrientations;

  const View({Key key, this.preferredOrientations}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ViewState<T>(createViewModel());
  }

  T createViewModel();

  @protected
  Widget getBusyWidget(BuildContext context, T viewModel) {
    switch (viewModel.asyncState) {
      case AsyncState.loading:
      case AsyncState.saving:
        return BusyWidget();
      default:
        return null;
    }
  }

  @protected
  Widget build(BuildContext context, T viewModel);

  Widget _build(BuildContext context, T viewModel) {
    SystemChrome.setPreferredOrientations(preferredOrientations);

    Widget busyWidget = getBusyWidget(context, viewModel);

    if (busyWidget != null) {
      return busyWidget;
    }

    return build(context, viewModel);
  }
}

class _ViewState<T extends ViewModel> extends State<View<T>>
    with SingleTickerProviderStateMixin {
  VoidSupplier _listener;

  final ViewModel viewModel;

  _ViewState(this.viewModel);

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    viewModel.vsync = this;

    viewModel.init(context);

    setListener(_listener = () {
      if (this.mounted) {
        this.setState(() {
          uuid = Uuid().v4().toString();
        });
      }
    });

    viewModel.addListener(_listener);

    runLater(() {
      viewModel.afterInit(context);
      viewModel.afterReload(context);
    });

    WidgetsBinding.instance.addPostFrameCallback(
      (callback) => viewModel.afterBuild(context),
    );
  }

  // TODO: I have a bad understanding on how listeners work
  // when I know more, this can be removed, and a listener only added once
  // on state init
  setListener(VoidCallback listener) {
    if (_listener != null) {
      viewModel.removeListener(_listener);
    }
    _listener = listener;
    viewModel.addListener(_listener);
  }

  // TODO: I have a bad understanding of setState, putting a UID in here
  // whenever I want setState to do a rebuild
  String uuid;

  @override
  void reassemble() {
    // TODO: remove this duplication once I know what is going on
    // with the listeners
    setListener(_listener = () {
      if (this.mounted) {
        this.setState(() {
          uuid = Uuid().v4().toString();
        });
      }
    });

    viewModel.afterReload(context);
    super.reassemble();
  }

  @override
  Widget build(BuildContext context) {
    viewModel.isCurrent = ModalRoute.of(context).isCurrent;
    return widget._build(context, viewModel);
  }
}

@protected
Widget visibleIf({
  bool condition,
  @required SupplierOf<Widget> builder,
  SupplierOf<Widget> elseBuilder,
}) {
  if (!condition) {
    if (elseBuilder != null) {
      return elseBuilder();
    }

    return SizedBox.shrink();
  }

  return builder();
}

@protected
List<Widget> allVisibleIf({
  bool condition,
  @required SupplierOf<List<Widget>> builder,
}) {
  if (!condition) {
    return [];
  }

  return builder();
}
