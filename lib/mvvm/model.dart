import 'package:json_annotation/json_annotation.dart';

abstract class Model {
  String id;
  int created;
  int updated;
  int deleted;
  @JsonKey(ignore: true)
  bool $isFromCache;
  @JsonKey(ignore: true)
  bool $isAtServer;
  @JsonKey(ignore: true)
  bool $hasPendingWrites;

  Map<String, dynamic> toJson();
}

bool Function(Model element) idEquals(String id) =>
    (Model model) => model.id == id;

bool Function(Model element) idNotEquals(String id) =>
    (Model model) => model.id != id;
