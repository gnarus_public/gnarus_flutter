import 'package:flutter/material.dart';
import 'package:gnarus_flutter/check.dart';
import 'package:uuid/uuid.dart';
import 'dart:async';

import 'async_state.dart';

abstract class ViewModel extends ChangeNotifier {
  AsyncState $asyncState = AsyncState.ready;
  bool isCurrent = false;
  Map<String, StreamSubscription> subscriptions = Map();
  SingleTickerProviderStateMixin vsync;

  @override
  void dispose() {
    for (StreamSubscription subscription in this.subscriptions.values) {
      subscription.cancel();
    }
    super.dispose();
    this.disposed = true;
  }

  AsyncState get asyncState => $asyncState;

  set asyncState(AsyncState value) => $asyncState = value;

  bool get busy => asyncState != AsyncState.ready;

  @protected
  void ready() => this.asyncState = AsyncState.ready;

  @protected
  void loading() => this.asyncState = AsyncState.loading;

  @protected
  void saving() => this.asyncState = AsyncState.saving;

  void init(BuildContext context) {}

  void afterInit(BuildContext context) {}

  void afterBuild(BuildContext context) {}

  @mustCallSuper
  void afterReload(BuildContext context) {
    notifyListeners();
  }

  bool disposed = false;

  @override
  @mustCallSuper
  void notifyListeners() {
    if (this.disposed) {
      return;
    }

    super.notifyListeners();
  }

  @protected
  void notifyOnEmit<T>({
    @required Stream<T> stream,
    String tag,
    bool cancelExisting = false,
  }) {
    checkNotNull(stream);

    if (tag == null) {
      tag = Uuid().v4();
    }

    if (subscriptions.containsKey(tag)) {
      if (cancelExisting) {
        subscriptions[tag].cancel();
        subscriptions.remove(tag);
      } else {
        return;
      }
    }

    subscriptions[tag] = stream.listen((_) => notifyListeners());
  }
}
