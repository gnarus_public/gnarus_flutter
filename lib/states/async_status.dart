class AsyncStatus {
  static const String LOADING = "LOADING";
  static const String READY = "READY";
  static const String SAVING = "SAVING";
}
