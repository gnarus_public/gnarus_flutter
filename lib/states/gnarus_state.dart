import 'dart:async';

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

import 'async_status.dart';

typedef ViewBuilder = Widget Function(BuildContext context);

abstract class GnarusState<T extends StatefulWidget> extends State<T> {
  final formKey = GlobalKey<FormState>();

  FormState get form => formKey.currentState;

  String _status = AsyncStatus.LOADING;
  BehaviorSubject<String> _subject = BehaviorSubject.seeded(AsyncStatus.LOADING);

  @protected
  List<StreamSubscription> subs = List();
  ViewBuilder viewBuilder;

  set status(value) => _status = value;

  get status => _status;

  get busy => [AsyncStatus.LOADING, AsyncStatus.SAVING].contains(status);

  ValueObservable<String> get stream => _subject.stream;

  GnarusState create(ViewBuilder viewBuilder) {
    this.viewBuilder = viewBuilder;
    this.onCreate();
    return this;
  }

  void listen<R>(Stream<R> stream, void Function(R value) handler) {
    this.subs.add(stream.listen((value) {
      handler(value);
      afterListenerEmit();
    }));
  }

  Future<void> afterListenerEmit() async {}

  Future<void> onCreate() async {}

  @override
  Widget build(BuildContext context) => viewBuilder(context);

  @override
  void dispose() {
    super.dispose();
    this.subs.forEach((subs) => subs.cancel());
    this._subject.close();
  }

  @override
  void setState(VoidCallback fn) {
    super.setState(fn);
    if (_subject.value != this.status) {
      _subject.add(this.status);
    }
  }
}
