/// Throws an [ArgumentError] if the given [expression] is `false`.
///
/// Optional arguments [name] and [message] are applied to the
/// [ArgumentError] if provided.
void checkArgument(bool expression, {String name, String message}) {
  if (!expression) {
    throw ArgumentError.value(
        null, name, message ?? 'check expression failed');
  }
}

/// Throws an [ArgumentError] if the given [reference] is `null`.
///
/// Optional argument [name] is applied to the [ArgumentError] if provided.
void checkArgumentNotNull(reference, [String name]) {
  if (reference == null) {
    throw ArgumentError.notNull(name);
  }
}

/// Throws a [Index] if the given [indexable] is `null` or has a length of `0`.
///
/// Optional arguments [name] and [message] are applied to the [ArgumentError] if
/// provided.
void checkArgumentNotEmpty(indexable, {String name, String message}) {
  final size = indexable?.length ?? 0;
  if (size == 0) {
    throw IndexError(
      0,
      indexable,
      name,
      message ?? 'cannot be empty',
    );
  }
}

/// Throws an [ArgumentError] if the given [iterable] contains any elements that 
/// are `null`.
///
/// Optional arguments [name] and [message] are applied to the [ArgumentError] if 
/// provided.
void checkArgumentContainsNoNulls(iterable, {String name, String message}) {
  if (iterable == null) {
    return; // A null iterable does not contain any nulls
  }

  if (iterable is List && iterable.any((i) => i == null)) {
    throw ArgumentError.value(iterable, name, message ?? 'cannot contain null entries');
  }

  if (iterable is Map && (iterable.values.any((i) => i == null))) {
    throw ArgumentError.value(iterable, name, message ?? 'cannot contain null entries');
  }
}

/// Throws a [StateError] if the given [expression] is `false`.
///
/// Optional argument [message] is applied to the [StateError] if provided.
void check(bool expression, {String message}) {
  if (!expression) {
    throw StateError(message ?? 'check expression failed');
  }
}

/// Throws a [NullThrownError] if the given [reference] is `null`.
void checkNotNull<T>(reference) {
  if (reference == null) {
    throw NullThrownError();
  }
}

/// Throws a [StateError] if the given [indexable] is `null` or has a length of `0`.
///
/// Optional argument [message] is applied to the [StateError] if provided.
void checkNotEmpty(indexable, {String message}) {
  final size = indexable?.length ?? 0;

  if (size == 0) {
    throw StateError(message ?? 'cannot be empty');
  }
}

/// Throws a [StateError] if the given [iterable] contains any elements that are `null`.
///
/// Optional argument [message] is applied to the [StateError] if provided.
void checkContainsNoNulls(iterable, {String message}) {
  if (iterable == null) {
    return; // A null iterable does not contain any nulls
  }

  if (iterable is List && iterable.any((i) => i == null)) {
    throw StateError(message ?? 'map values cannot contain null');
  }

  if (iterable is Map && (iterable.values.any((i) => i == null))) {
    throw StateError(message ?? 'map values cannot contain null');
  }
}




/// Returns `true` if [iterable] is `null` or has `length` = `0`.
/// Otherwise returns `false`.
/// 
/// Throws [ArgumentError] if [iterable] is not `null` and has type other than
/// [List], [Map] or [String]
bool isNullOrEmpty(iterable) {
  if (iterable == null) {
    return true;
  } else if (iterable is List || iterable is Map || iterable is String) {
    return iterable.length == 0;
  } else {
    throw ArgumentError('length not available for type: ${iterable.runtimeType}');
  }
}

/// Returns true if [iterable] contains any elements that are `null`.
bool containsNull(iterable) {
  if (iterable == null) {
    return false;  // A null iterable does not contain any nulls
  }
  if (iterable is List && iterable.any((i) => i == null)) {
    return true;
  }

  if (iterable is Map && (iterable.values.any((i) => i == null))) {
    return true;
  }

  return false; // Does not contain anything if it is not a list or map
}

/// Returns `true` if [reference] is `null`.
/// 
/// This method is a helper for filtering null from collections.
/// e.g. list.removeWhere(isNull)
bool isNull(reference) => reference = null;

/// Returns `true` if [reference] is not `null`.
/// 
/// This method is a helper for filtering null from collections.
/// e.g. list.where(isNotNull)
bool isNotNull(reference) => reference != null;