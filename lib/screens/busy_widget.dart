import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class BusyWidget extends StatelessWidget {
  final bool busy = true;
  final double backgroundOpacity;
  final Color backgroundColor;
  final Color foregroundColor;
  final WidgetBuilder builder;
  final double height;

  BusyWidget({
    Key key,
    this.builder,
    this.backgroundOpacity = 1,
    this.backgroundColor = Colors.white,
    this.foregroundColor = Colors.black,
    this.height = double.infinity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SizedBox(
      height: height,
      width: double.infinity,
      child: Stack(
        children: [
          builder != null ? builder(context) : SizedBox.expand(),
          Opacity(
            opacity: backgroundOpacity,
            child: ModalBarrier(
              dismissible: false,
              color: backgroundColor,
            ),
          ),
          Center(
            child: SpinKitWave(
              key: Key('progress_spinner'),
              size: 30,
              color: foregroundColor,
              type: SpinKitWaveType.center,
            ),
          ),
        ],
      ),
    );
}
