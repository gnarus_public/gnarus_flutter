import 'package:flutter/material.dart';

import '../states/gnarus_state.dart';

abstract class GnarusWidget<T extends GnarusState> extends StatefulWidget {
  final T state;

  const GnarusWidget(this.state, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => state.create(this.build);

  void dispose() {

  }
  
  Widget build(BuildContext context);
}
