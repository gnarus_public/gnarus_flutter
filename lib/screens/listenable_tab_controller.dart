import 'package:flutter/material.dart';

class ListenableTabController extends StatefulWidget {
  const ListenableTabController(
      {Key key, @required this.length, this.initialIndex = 0, @required this.child, this.onIndexChanged})
      : assert(initialIndex != null),
        assert(length >= 0),
        assert(initialIndex >= 0 && initialIndex < length),
        super(key: key);

  final Widget child;
  final int initialIndex;
  final int length;
  final void Function(int newIndex, int oldIndex) onIndexChanged;

  @override
  _DefaultTabControllerState createState() => _DefaultTabControllerState();
}

class _DefaultTabControllerState extends State<ListenableTabController> with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(
      vsync: this,
      length: widget.length,
      initialIndex: widget.initialIndex,
    );
    if (widget.onIndexChanged != null) {
      _controller.addListener(() {
        widget.onIndexChanged(_controller.index, _controller.previousIndex);
      });
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _TabControllerScope(
      controller: _controller,
      enabled: TickerMode.of(context),
      child: widget.child,
    );
  }
}

class _TabControllerScope extends InheritedWidget {
  const _TabControllerScope({
    Key key,
    this.controller,
    this.enabled,
    Widget child,
  }) : super(key: key, child: child);

  final TabController controller;
  final bool enabled;

  @override
  bool updateShouldNotify(_TabControllerScope old) {
    return enabled != old.enabled || controller != old.controller;
  }
}
