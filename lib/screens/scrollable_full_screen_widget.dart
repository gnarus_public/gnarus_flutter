import 'package:flutter/material.dart';

class ScrollableFullScreen extends StatelessWidget {
  final Widget portrait;
  final Widget landscape;

  ScrollableFullScreen({this.portrait, this.landscape});

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (context, orientation) => LayoutBuilder(
            builder: (context, viewportConstraints) => SingleChildScrollView(
                    child: ConstrainedBox(
                  constraints:
                      BoxConstraints(minHeight: viewportConstraints.maxHeight, maxWidth: viewportConstraints.maxWidth),
                  child:
                      orientation == Orientation.landscape && this.landscape != null ? this.landscape : this.portrait,
                )),
          ),
    );
  }
}
